# Projet GO

## Mise en ligne 

### Installation de GO

### Mise des variable d'env

Met en place recherche des programes de go

```bash
root@srv453542:~# cat .bash_profile 
export GOROOT=/usr/local/go
export GOPATH=$HOME/go
export PATH=$GOROOT/bin:GOPATH/bin:$PATH

export PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/snap/bin:/usr/local/go/bin
```


Lancer : go run cmd/main.go
Dans le fichier :  /tmp/crontab.tuTDEF/crontab 
La ligne permet de relancer la commande en cas d'arret toutes les minutes
* * * * * cd /var/www/go/ && /usr/local/go/bin/go run cmd/main.go



## GO
### Description

#### Caractéristique
Go ou golang est langage compilé et concurrent créer par Google en 2009. Go dispose d'un Garbage Collector, qui permet de libérer automatiquement la mémoire qui n'est plus utilisée par le programme. Cela permet de réduire les erreurs de mémoire et d'améliorer les performances du programme. Le langage est  statiquement typé, ce qui signifie que les types de données des variables doivent être définis à la compilation. Go est également fortement typé, ce qui signifie qu'il n'y a pas de conversion implicite entre les types de données.

#### Point fort pour une api Rest

**Facilité de développement**
Le golang dispose d'une syntaxe simple et clair, et un ajout facile de bibliothèque externe.  
**Rapidité d'execution** : C'est un langage compilé ce qui rend le langage léger pour le development car il n'y a pas besoin d'avoir un émulateur de serveur web pour fonctionner.
**Mise en ligne facile** : Il suffit juste d'installer golang sur le serveur, préciser le port sur lequel on veut que le programme tourne. Le langage dispose native de packet HTTP.  
**Sécurité** : Fonctionnalité de garbage collecteur intégré qui évité les fuite de mémoires.

### Paradigme
**Langage compilé** : C'est un langage de programmation qui est traduit en un langage machine, c'est-à-dire en langage binaire, avant d'être exécuté par la machine. Cela signifie que le langage compilé est directement interprété par la machine, sans passer par une étape d'interprétation. Ces langages sont donc plus proche de la machine est général dispose de meilleurs performances.

**Programmation concurrente** : C'est une approche de programmation qui permet d'exploiter au mieux les ressources d'une machine en exécutant plusieurs tâches en parallèle. Cela peut être réalisé en utilisant plusieurs threads (ou processus) qui s'exécutent simultanément sur différents coeurs d'une machine. Cela peut améliorer considérablement les performances d'un programme, en particulier dans les domaines où les tâches sont indépendantes les unes des autres et peuvent être exécutées en parallèle.

En Go, la programmation concurrente est réalisée en utilisant des goroutines. Une goroutine est une unité de concurrence légère et indépendante, qui peut être exécutée en parallèle avec d'autres goroutines.  

**Programmation impérative et structuré** : c'est une approche de programmation qui se concentre sur la description de l'exécution d'un programme en détail, en utilisant des instructions et des structures de contrôle pour spécifier comment les données doivent être manipulées et comment les opérations doivent être effectuées.


### Architecture

#### Préambule
Golang dispose d'un grande liberté dans l'architecture, je me suis inspiré de me connaissance d'autre langage Symfony et nestJs pour réaliser l'architecture plus de la lecture d'article sur internet (https://grafana.com/blog/2024/02/09/how-i-write-http-services-in-go-after-13-years/);
Voici la liste de mes dossiers

### CMD
Ce dossiers contient *main.go*, il est le point d'entre du programme.
```go
package main

import (
	"api/config"
	"api/helpers"
	"api/migration"
	"api/routes"
	"os"

	"github.com/gin-gonic/gin"
)

func main() {
	r := gin.Default()

	helpers.ApplyCORSConfig(r)

	DB := config.ConnectDB()

	// Migration
	migration.MigrateDatabase(DB)

	// Initialize routes
	routes.SetupRoutes(r, DB)

	r.Run(os.Getenv("API_PORT"))
}
```
#### Instance de GIN
J'ai gardé ce fichier aussi petit que possible.
La ligne de code **r := gin.Default()** est utilisée dans le contexte du framework web Go appelé Gin.
gin.Default() crée une instance du moteur HTTP Gin avec certaines configurations par défaut. Cela inclut l'ajout de middlewares tels que la gestion des erreurs, la gestion des CORS (Cross-Origin Resource Sharing), ainsi que la configuration du mode de routage.
r est une variable de type *gin.Engine qui représente l'instance du moteur Gin créée. Cette variable peut être utilisée pour configurer des routes, gérer les requêtes HTTP

#### Application dec CORS
La ligne **helpers.ApplyCORSConfig(r)** appele le package *helpers* et la function *ApplyCORSConfig* avec le paramètre *r* qui est l’instance déclaré plutôt.
```go
func ApplyCORSConfig(r *gin.Engine) {
	corsConfig := cors.Config{
		AllowOrigins:     []string{"http://maxence-bossin.eu"},
		AllowMethods:     []string{http.MethodGet, http.MethodPatch, http.MethodPost, http.MethodHead, http.MethodDelete, http.MethodOptions},
		AllowHeaders:     []string{"Content-Type", "X-XSRF-TOKEN", "Accept", "Origin", "X-Requested-With", "Authorization", "Access-Control-Allow-Credentials", "withcredentials"},
		ExposeHeaders:    []string{"Content-Length"},
		AllowCredentials: true,
		MaxAge:           12 * time.Hour,
	}
	r.Use(cors.New(corsConfig))
}
``` 
Le CORS va définir les Origine de provenance de l'appel de l'api http://maxence-bossin.eu qui correspond à l'adresse de mon front end. On va également autorisé les méthode HTTP GET, PATCH, POST, DELETE, OPTION.
On va également autorisé des headers par exemple "Authorization" est l'endroit par le quel mon front va stocker le cookie.

#### Connexion à la Base de données postgres
Appel le packet config et la function *ConectDB*
```go

var DB *gorm.DB

func getDSN() string {
	print("Loading .env file...")
	err := godotenv.Load(".env")
	if err != nil {
		fmt.Print("Error loading .env file")
	}

	print("Get Database DNS")
	dbServer := os.Getenv("DB_SERVER")
	dbPort := os.Getenv("DB_PORT")
	dbUser := os.Getenv("DB_USER")
	dbPassword := os.Getenv("DB_PASSWORD")
	dbName := os.Getenv("DB_NAME")
	timeZone := "Europe/Paris"

	return fmt.Sprintf("host=%s user=%s password=%s dbname=%s port=%s sslmode=disable TimeZone=%s",
		dbServer, dbUser, dbPassword, dbName, dbPort, timeZone)
}

func ConnectDB() *gorm.DB {
	print("Connecting to database...")
	dsn := getDSN()
	db, err := gorm.Open(postgres.Open(dsn), &gorm.Config{})
	if err != nil {
		panic("failed to connect database")
	}
	DB = db
	return db
}
```
Sur ce ficher plusieurs choses sont notables. 
1) **os.Getenv("DB_SERVER")** permet d'appeler les variable d'environnement qui sont mise en gitignore pour ne pas exposer mes identifiants sur gitlab
2) La notion de privé, public: En go une function ou une variable sont exportable vers d'autres ficher si elle commence par une majuscule, sinon c'est privé. La function *getDNS()* est privé et ne pourra qu'être utilisé dans ce fichier.

#### Fichier de migration

*migration.MigrateDatabase(DB)* cette ligne réalise l'ensemble des migrations présente dans le fichier. Le fichier il faut déclare l'ensemble des models et ensuite grace l'orm va automatiquement produire les requêtes sql (Postgres dans mon cas). (todo mettre liens vers explication du fichier model)

```go
func MigrateDatabase(db *gorm.DB) {

	db.AutoMigrate(&models.User{})
	db.AutoMigrate(&models.Messenger{})

	db.AutoMigrate(&models.Dish{})
	db.AutoMigrate(&models.Allergen{})
	db.AutoMigrate(&models.DishAllergen{})

	db.Set("gorm:table_options", "ENGINE=InnoDB")
}
```

#### Déclaration des points d'entrée
*routes.SetupRoutes(r, DB)*

Appel l'ensemble des routes

```go

func SetupRoutes(r *gin.Engine, db *gorm.DB) {

	AuthRouter(r, db)
	MessengerRouter(r, db)
	DishRouter(r, db)
    ...

}
```

#### Démarrer le serveur
*r.Run(os.Getenv("API_PORT"))*
Indique sur quel port le serveur doit être écouté.
