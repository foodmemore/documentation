# Sommaire

1. **Introduction**
    1. [Portée du rapport](#1-portée-du-rapport)
    2. [Contexte et objectifs du projet](#2-contexte-et-objectifs-du-projet)
    3. [Fonctionnalité pour les professionnels de la cuisine](#3-fonctionnalité-pour-les-professionnels-de-la-cuisine)
    4. [Fonctionnalité pour les particuliers](#4-fonctionnalité-pour-les-particuliers)
    5. [Fonctionnalité de la gestion administrative](#5-gestion-de-la-partie-administrative)

2. **Analyse des besoins**
    - Étude des besoins fonctionnels et non fonctionnels
    - Analyse des utilisateurs et de leurs attentes
    - Identification des fonctionnalités requises

3. **Choix des technologies et outils utilisés**
    - Outil de gestion de projet
    - Langages de programmation (front-end, back-end)
    - Frameworks et bibliothèques
    - Base de données
    - Hébergement et serveur web

4. **Conception du site**
    - Wireframes et maquettes
    - Schéma de base de données
    - Architecture du front-end
    - Architecture du back-end
    - Définition de l'expérience utilisateur (UX)
    - Choix des couleurs, typographies et charte graphique

5. **Développement**
    - Mise en place
    - Intégration des fonctionnalités
    - Tests et débogage
    - Documentation du code

6. **Mise en production**
    - Stratégie de déploiement
    - Configuration du serveur
    - Gestion de la sécurité
    - Surveillance et maintenance

7. **Formation et support**
    - Formation des utilisateurs finaux
    - Procédures de gestion des incidents

8. **Évaluation du projet**
    - Réalisation cahier des charges
    - Identification des points forts et des axes d'amélioration

9. **Conclusion**
    - Récapitulation des principales conclusions
    - Perspectives d'évolution du projet

10. **Remerciements**
    - Reconnaissance envers les personnes ayant contribué au projet
    - Mention des ressources utilisées et des organismes partenaires

11. **Annexes**
    - Documents complémentaires (ex. : diagrammes UML, spécifications techniques détaillées)
    - Glossaire des termes techniques


## 1. Sommaire

### 1 Portée du rapport
Dans le cadre de la préparation de la certification professionnel : *TP  Concepteur développeur d'applications* .   
Ce titre atteste les compétences suivantes :
#### Développer une application sécurisée 
- Installer et configurer son environnement de travail en fonction du projet.
- Développer des interfaces utilisateur
- Développer des composants métier
- Contribuer à la gestion d'un projet informatique
#### Concevoir et développer une application sécurisée organisée en couches
- Analyser les besoins et maquetter une application
- Définir l'architecture logicielle d'une application
- Concevoir et mettre en place une base de données relationnelle
- Développer des composants d'accès aux données SQL et NoSQL
#### Préparer le déploiement d'une application sécurisée
- Préparer et exécuter les plans de tests d'une application
- Préparer et documenter le déploiement d'une application
- Contribuer à la mise en production dans une démarche DevOps


### 2 Contexte et objectifs du projet

#### Contexte

Création d'une solution SaaS (software as service) sous forme d'application web responsive. La platforme devra mettre en relation des chefs cuisinier à domicile et des clients. Les clients pourront consulter les menus créer par les chefs et les choisir à des dates spécifiques. L'objectif est de faciliter la reservation des repas fait maison par des chefs cuisiniers sans aller au restaurant. 
L'utilisateur pourra faire des recherche en fonction de ces préférence culinaire et de la date à laquelle il souhaite commander.
Le cuisiniers déposera ces menus en indiquant leurs disponibilité : Date et quantité; leurs prix; leurs ingrédients.

#### Conception requis 
* La solution devra utiliser des framework pour le front et le backend
* Le backend devra être réalisé sous forme d'api REST
* Le code produit devra être tester
* Le frontend devra suivre les bonnes pratique de développement et d’accessibilité 
* La base de données devra être relationnel
* Un système de connexion sécurisé devra être mis en place
* Le serveur devra être sécurisé
* Le déploiement devra être automatisé

### 3 Fonctionnalité pour les professionnels de la cuisine

#### Gestion des menus
Les chefs cuisiniers pourront créer, modifier et supprimer les menus.
Un menu se compose d'au moins un plat et d'une entrée et ou d'un desert.    
Il pourront programmer à l'avance leurs menu (la quantité max possible) ainsi les jours au quels les menus sont disponibles.

#### Gestion des horaires
Le chef défiera les jours de disponibilité ainsi que les heures aux quelle les utilisateurs pourront chercher les menus.

#### Gestion partie commande
Ensemble des vue des commande et leurs état, en cours, terminées, annulées.
Une vue détail de la commande sera disponible avec la possibilité de communiquer au client ainsi que de la possibilité de gérer les factures.


### 4 Fonctionnalité pour les particuliers
#### Recherche de menu
Le particulier pourra filter les offre en fonction de la date, du prix des allergène
#### Reservation de menu
Sur le détail d'un menu pourra ajouter a son panniers et définir la qualité voulu.
Il aura également une vue commande diviser en 2 partie en cours et historique :    
    - en cours il pourra communiquer avec le chef pour préciser ça demande    
    -  historique : pouvoir voir les prix des ancienne commande.
#### Système de payement
Le particulier pourra payer la commande de son panniers via stipe
#### Demander à devenir cuisinier
Un utilisateur si il présente les documents nécessaire pourra devenir cuisinier sous vérification de l’administrateur. Il disposera de la possibilité de changer de role et de choisir si il utilise l'application en tant qui cuisinier ou en tant que qu’utilisateur

### 5. Gestion de la partie administrative
#### Agrément des professionnel 
La partie administrative pourra vérifier si oui ou non les cuisinier est adapter à proposer des menu sur la platform
#### Vu du chiffre d'affaire
Mise en place de statistique corespondent à l'ensemble des commandes stripe


## 2 Analyse des besoins

## 3 Choix des technologies et outils utilisés

### 3.1 Outil de gestion de projet

