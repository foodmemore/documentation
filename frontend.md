# Foodmemore frontend : Angular

## description angular

Version utilisé : 17

## Installation

## Architecture du projet

### Core

#### Les services gateways (Appel de l'api rest)

#### L'intercepteur HTTP
La propriété XMLHttpRequest.withCredentials est un booléen indiquant si une requête cross-origin (entre plusieurs sites) doit inclure des informations d'authentification telles que des cookies
Par default l'option est à false il faut donc la mettre à true HTTP `withCredentials: true` 

Le cookie envoyé par le serveur backend est en mode HTTP. Pour gérer cela, Angular utilise des intercepteurs HTTP qui vont cloner la requête et modifier l'en-tête HTTP. Pour la route de connexion (login), cela permet d'utiliser set-Cookie et de conserver automatiquement le cookie dans un autre endroit. Cette méthode aide à prévenir les risques d'attaques XSS sur le jeton de connexion.

```ts
import { HttpInterceptorFn } from "@angular/common/http";

export const authInterceptor: HttpInterceptorFn = (req, next) => {
  const clonedRequest = req.clone({ withCredentials: true });
  return next(clonedRequest);
};
```

### Shared

### Système de routing

Angular fonctionne en mode Single Page Application (SPA). Un mode SSR (Server-Side Rendering) est disponible dans les dernières versions, mais il n'est pas encore complètement abouti. Pour éviter un chargement initial trop lourd, le routeur d'Angular permet le chargement différé (lazy loading). Les ressources ne sont chargées que lorsque l'utilisateur en a besoin, ce qui améliore la vitesse de chargement initial.

Le fichier à la racine charge les routes via un provider 
```ts
import { routes } from './app.routes';

export const appConfig: ApplicationConfig = {
  providers: [
    provideRouter(routes),
    ...
  ],
};
```
mporte le fichier contenant toutes les routes de l'application. Ce fichier définit les chemins d'accès (path) pour les différentes URL et configure les redirections. Par exemple, si un utilisateur arrive sur le site ou tente d'accéder à une URL inexistante, il est redirigé vers le menu principal, qui présente une liste des sections du site accessibles sans connexion. La propriété loadChildren permet de charger les routes en arrière-plan. Cela présente un double avantage : maintenir un chargement initial rapide tout en assurant une navigation fluide en chargeant les autres routes en arrière-plan.

```ts
import { Routes } from '@angular/router';

export const routes: Routes = [
  {
    path: '',
    redirectTo: 'menu',
    pathMatch: 'full',
  },

  {
    path: 'menu',
    loadChildren: () =>
      import('./routes/menu/menu.routes').then((r) => r.MENU_ROUTES),
  },

  {
    path: '',
    loadChildren: () =>
      import('./routes/auth/auth.routes').then((r) => r.AUTH_ROUTES),
  },
  ...
  {
    path: '**',
    redirectTo: 'menu',
    pathMatch: 'full',
  },
];
```
Exemple de route enfant : Routes d'authentification

On va acceder à la page de connexion via la route '/connexion' à noté que si le parent avait un path la route en aurait hérité.
La route va ensuite appeler le composant qui correspond à la page souhaité  
Ce n'est pas obligatoire mais je préfère nommer ces composant avec le mot Route pour savoir que c'est composant servent à ça.

```ts
import { Routes } from '@angular/router';

import { LoginRouteComponent } from './login/login.component';
import { RegisterRouteComponent } from './register/register.component';
export const AUTH_ROUTES: Routes = [
  {
    path: 'connexion',
    component: LoginRouteComponent,
  },
  {
    path: 'inscription',
    component: RegisterRouteComponent,
  },
];

```




## Utilisation des stores

### Explication et intérêt

La responsabilité des stores est de gérer l'état et les mutations de l'application. Au lieu de gérer les variables via les composants, elles sont maintenant gérées directement par les stores. Pour Angular, cela permet d'afficher les changements de variables sans passer par des @input ou @output, facilite la traçabilité et l'intégrité des données.

Actuellement, Angular utilise Zone.js pour détecter les changements dans l'application. Cependant, dès qu'il y a un changement, tous les composants visibles sur la page sont rappelés, même si ces composants ne utilisent pas les données concernées. Cela peut entraîner des performances dégradées.

Le mode "onPush" est une stratégie de détection de changement utilisée par Angular pour déterminer si un composant doit être rendu à nouveau. Lorsque le mode "onPush" est activé, Angular compare les valeurs des propriétés d'entrée du composant au moment du rendu précédent avec les valeurs actuelles. Si aucune des propriétés d'entrée n'a changé, Angular ne rend pas à nouveau le composant.

En utilisant le mode "onPush" en combinaison avec Zone.js, vous pouvez améliorer les performances de votre application en réduisant le nombre de cycles de rendu inutiles. Cela permet de limiter les appels à la fonction de rendu des composants et de réduire ainsi le temps de traitement, tout en évitant les doublons et en restant clair dans la gestion de l'état.

### Bibliotèque utilisé

La bibliothèque utilisé est : NGRX, elle s'inspire de la bibliothèque REDUX (react)  


#### Concepts clés

- Les actions décrivent des événements uniques qui sont envoyés depuis les composants et les services.
- Les changements d'état sont gérés par des fonctions pures appelées réducteurs qui prennent l'état actuel et la dernière action pour calculer un nouvel état.
- Les sélecteurs sont des fonctions pures utilisées pour sélectionner, dériver et composer des morceaux d'état.
- L'état est accessible via le Store, un observable d'état et un observateur d'actions.
